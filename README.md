### CV templates

You can find my **single page** and **academic** CV LaTex templates in this repo.

Compile with pdfLaTeX.

Feel free to contact if you have any questions.
